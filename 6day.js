let users = [];
let tasks = [];
function checkUser(name)
{
    let count = false;
    for(let key in users)
    {
        if(key == name)
        {
            count = true;
        }
    };
    if(count) return true;
    else return false;
}
function checkTask(name)
{
    let count = false;
    for(let key in tasks)
    {
        if(key == name)
        {
            count = true;
        }
    };
    if(count) return true;
    else return false;
}
function addUser(firstName, lastName)
{
    let str = firstName + ' ' + lastName;
    users[str] = {
        firstName: firstName,
        lastName: lastName,
        createdAt: Date.now(),
        tasks: []
    };
}
function addTusk(from, title, description, deadline, responsible)
{
    if(checkUser(from))
    {
        if(!checkUser(responsible))
        {
            responsible = from;
        }
        tasks[title] = {
            author: from,
            responsible: responsible,
            title: title.slice(0, 20),
            description: description.slice(0,200),
            createdAt: Date.now(),
            deadline: Date.parse(deadline),
            status: 'Wait'
            };
        users[responsible].tasks++;
        return
    }
}
function setResponsible(title, responsible)
{
    if(checkTask(title) && checkUser(responsible))
    {
        users[tasks[title].responsible].tasks--;
        users[responsible].tasks++;
        tasks[title].responsible = responsible;
        console.log(`Ответственный за задачу: ${title} изменен на: ${responsible}`)
    }
}
function setDeadline(title, deadline)
{
    if(checkTask(title))
    {
        tasks[title].deadline = Date.parse(deadline);
        console.log(`Дедлайн задачи: ${title} изменен на: ${deadline}`)
    }
}
function setStatus(title, status)
{
    if(checkTask(title))
    {
        tasks[title].status = status;
        console.log(`Статус задачи: ${title} изменен на: ${status}`)
    }
}
function showUsers()
{
    for(let key in users)
    {
        console.log(users[key].firstName + ' ' + users[key].lastName + ' '+ users[key].createdAt + ' ' + users[key].tasks);
    }
}
function showTasks(status)
{
    switch(status)
    {
        case undefined:
        for(let key in tasks)
        {
            console.log(tasks[key].author + ' '+ tasks[key].responsible + ' '+ tasks[key].title + ' ' 
                + tasks[key].description + ' '+tasks[key].createdAt+ ' ' + tasks[key].deadline + ' ' +
                tasks[key].status);
        }
        break;
        case 'Wait':
        for(let key in tasks)
        {
            if(tasks[key].status == 'Wait')
            {
                console.log(tasks[key].author + ' '+ tasks[key].responsible + ' '+ tasks[key].title + ' ' 
                + tasks[key].description + ' '+tasks[key].createdAt + ' ' + tasks[key].deadline + ' ' +
                tasks[key].status);
            }
        
        }
        break;
        case 'In Progress':
        for(let key in tasks)
        {
            if(tasks[key].status == 'In Progress')
            {
                console.log(tasks[key].author + ' '+ tasks[key].responsible + ' '+ tasks[key].title + ' ' 
                + tasks[key].description + ' '+tasks[key].createdAt + ' ' + tasks[key].deadline + ' ' +
                tasks[key].status);
            }
        
        }
        break;
        case 'Done':
        for(let key in tasks)
        {
            if(tasks[key].status == 'Done')
            {
                console.log(tasks[key].author + ' '+ tasks[key].responsible + ' '+ tasks[key].title + ' ' 
                + tasks[key].description + ' '+tasks[key].createdAt + ' ' + tasks[key].deadline + ' ' +
                tasks[key].status);
            }
        
        }
        break;
    }
}
addUser('Grisha', 'Pokuziyov');
addUser('Misha', 'Yakimov');
addUser('Pasha', 'Pupkin')
addTusk('Grisha Pokuziyov', 'Кушать', 'Много делать нужно', '2018-12-24', 'Misha Ya');
addTusk('Grisha Pokuziyov', 'Писать', 'Много делать нужно, но не нужно', '2018-12-24', 'Misha Ya');
addTusk('Grisha Pokuziyov', 'Спать', 'Много делать', '2018-12-13', 'Misha Yakimov');
setResponsible('Писать','Pasha Pupkin');
setDeadline('Спать','2019-12-24');
setStatus('Спать', 'In Progress');
showUsers();
showTasks();